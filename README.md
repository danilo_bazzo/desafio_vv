# Documentos

----
Acessar a pasta /documentos para instalação e evidências de testes 

* 1 - Instalação.docx
* 2 - Evidências e Testes.docx

## Resultado

Primeiramente quero agradecer ao Bruno e André por possibilitar a me candidadar a oportunidade na Via Varejo. 
Esse projeto foi um grande desafio não somente profissional mas também pessoal, foi bastante desafiador no qual não tinha muito conhecimento prático com 
com o MongoDB e Swagger, utilizei algumas técnicas no qual utilizei quando desenvolvia em .Net e algumas atuais que ainda estou me familiarizando.
Iniciei minha atividade em 09/03 e trabalhei todos esses dias até 12/03 por volta de 4 a 5h por dia. Devo ter levado umas 13 a 15h para desenvolver.

----
Att.
Danilo Bazzo