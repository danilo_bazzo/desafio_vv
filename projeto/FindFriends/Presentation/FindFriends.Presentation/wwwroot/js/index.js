﻿$(document).ready(function () {

    onLoadFriends = function (render, id, kmDistance) {
        var strId = (id != null) ? strId = '/' + id.replace('f', '') + '/' + kmDistance : '';

        $.ajax({
            type: "GET",
            url: "http://localhost/services/api/friends" + strId,
            headers:
            {
                'Token': getToken(),
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Headers': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", getToken());
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result);
                var html;
                for (var i = 0; i <= result.length; i++) {

                    html = '<div id="f' + result[i].id.toString() + '" class="col-md-4 friendRow">' +
                        '<h2>' + result[i].name + '</h2>' +
                        '<ul>' +
                        '<li>Amigos a: <a href="#" onclick=onClickFriend("f' + result[i].id.toString() + '",10)>10km</a> - ' +
                        '<a href="#" onclick=onClickFriend("f' + result[i].id.toString() + '",20)>20km</a> - ' +
                        '<a href="#" onclick=onClickFriend("f' + result[i].id.toString() + '",50)>50km</a>' +
                        '</li > ' +
                        '<li>Lat:' + result[i].currentCoordinate.latitude.toString() + ' | Long:' + result[i].currentCoordinate.longitude.toString() + '</li>' +
                        ((result[i].distanceFriend > 0) ? '<li class=dist>Distância do amigo: ' + result[i].distanceFriend.toFixed(2).toString() + ' Km</li>' : '') +
                        '</ul>' +
                        '<iframe width="250"' +
                        '        height="230"' +
                        '        frameborder="0"' +
                        '        style="border:solid 1px #000000"' +
                        '        src="https://maps.google.com/maps?q=' +
                        result[i].currentCoordinate.latitude + ',' +
                        result[i].currentCoordinate.longitude +
                        '&amp;width=500&amp;height=500&amp;hl=en&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed"></iframe>' +
                        '</div>';

                    $(render).append(html);
                }
            }
        });
    };

    getToken = function () {
        var dt = new Date();
        var ret = '';
        ret += dt.getFullYear();
        ret += getZeroLeft((dt.getMonth() + 1).toString());
        ret += getZeroLeft(dt.getDate());
        ret += getZeroLeft(dt.getHours());
        ret += getZeroLeft(dt.getMinutes());
        return ret;
    }

    getZeroLeft = function (vl) {
        var vlS = vl.toString();
        vlS = ('0' + vlS);
        vlS = vlS.substring(vlS.length - 2, 3);
        return vlS;
    }

    onClickFriend = function (o, d) {

        $(".friendRow").each(function (i) {
            if ($(this).attr('id') == o) {
                $("#resultInvisible").append($(this));
            }
        });

        $("#resultFriends").empty();
        $("#resultInit").empty();
        $("#resultInit").append($("#resultInvisible").children());
        $("#resultInit").find(".dist").hide();

        $("#fldNext").show();
        onLoadFriends($("#resultFriends"), o, d);
    };

    onLoadFriends($("#resultInit"), null, null);
    $("#fldNext").hide();


});
