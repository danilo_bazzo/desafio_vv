﻿using System;
using System.Collections.Generic;
using System.Text;
using FindFriends.Domain;

namespace FindFriends.Data.Model
{
    public class CoordinateBo : ICoordinate
    {
        public int idFriend { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
