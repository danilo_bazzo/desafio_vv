﻿using System;
using System.Collections.Generic;
using System.Text;
using FindFriends.Domain;

namespace FindFriends.Data.Model
{
    public class FriendBo : IFriend
    {
        public int id { get; set; }
        public String name { get; set; }
        public ICoordinate currentCoordinate { get; set; }
    }
}
