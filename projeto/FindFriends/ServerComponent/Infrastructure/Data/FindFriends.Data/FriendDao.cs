﻿using System;

using System.Data.SqlClient;
using System.Collections.Generic;
using FindFriends.Domain;
using FindFriends.Data.Model;

namespace FindFriends.Data
{
    public class FriendDao
    {
        private string _strConn;

        public FriendDao(string strConn)
        {
            _strConn = strConn;
        }

        public List<IFriend> list()
        {
            List<FriendBo> list = new List<FriendBo>();

            SqlConnection conn = new SqlConnection(_strConn);
            conn.Open();
            try
            {
                using (conn)
                {
                    SqlCommand command = new SqlCommand(@"
                    SELECT 
			                    F.ID,
			                    F.NAME,
			                    C.LATITUDE,
			                    C.LONGITUDE
                    FROM
			                    TB_FRIEND F
                    INNER JOIN	TB_COORDINATE C on C.ID_FRIEND = F.ID AND C.[CURRENT] = 1",
                    conn);


                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            list.Add(new FriendBo()
                            {
                                id = reader.GetInt32(0),
                                name = reader.GetString(1),
                                currentCoordinate = new CoordinateBo()
                                {
                                    latitude = reader.GetDouble(2),
                                    longitude = reader.GetDouble(3)
                                }
                            }
                            );
                        }
                    }
                    reader.Close();

                }
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return mappingDaoToDomainList(list);
        }

        public int insert(FriendBo item)
        {
            SqlConnection conn = new SqlConnection(_strConn);
            conn.Open();
            try
            {
                using (conn)
                {
                    SqlCommand cmdFriend = new SqlCommand(@"
                        INSERT INTO TB_FRIEND (NAME) VALUES (@NAME) SELECT SCOPE_IDENTITY()",
                    conn);

                    SqlParameter NAME = new SqlParameter("@NAME", System.Data.SqlDbType.VarChar);
                    NAME.Value = item.name;
                    cmdFriend.Parameters.Add(NAME);

                    item.id = Convert.ToInt32(cmdFriend.ExecuteScalar());

                    SqlCommand cmdCoordnateNew = new SqlCommand(@"
                        INSERT INTO TB_COORDINATE (ID_FRIEND, LATITUDE, LONGITUDE, [CURRENT]) VALUES (@ID_FRIEND, @LATITUDE, @LONGITUDE, 1)",
                    conn);
                    SqlParameter ID_FRIEND2 = new SqlParameter("@ID_FRIEND", System.Data.SqlDbType.Int);
                    ID_FRIEND2.Value = item.id;
                    cmdCoordnateNew.Parameters.Add(ID_FRIEND2);

                    SqlParameter LATITUDE = new SqlParameter("@LATITUDE", System.Data.SqlDbType.Float);
                    LATITUDE.Value = item.currentCoordinate.latitude;
                    cmdCoordnateNew.Parameters.Add(LATITUDE);

                    SqlParameter LONGITUDE = new SqlParameter("@LONGITUDE", System.Data.SqlDbType.Float);
                    LONGITUDE.Value = item.currentCoordinate.longitude;
                    cmdCoordnateNew.Parameters.Add(LONGITUDE);

                    return item.id;
                }
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
        }

        public void update(FriendBo item)
        {
            SqlConnection conn = new SqlConnection(_strConn);
            conn.Open();
            try
            {
                using (conn)
                {
                    SqlCommand cmdFriend = new SqlCommand(@"
                        UPDATE TB_FRIEND SET NAME = @NAME WHERE ID = @ID",
                    conn);

                    SqlParameter NAME = new SqlParameter("@NAME", System.Data.SqlDbType.VarChar);
                    NAME.Value = item.name;
                    cmdFriend.Parameters.Add(NAME);

                    SqlParameter ID = new SqlParameter("@ID", System.Data.SqlDbType.Int);
                    ID.Value = item.id;
                    cmdFriend.Parameters.Add(ID);

                    cmdFriend.ExecuteNonQuery();


                    SqlCommand cmdCoordnateOld = new SqlCommand(@"
                        UPDATE TB_COORDINATE SET [CURRENT] = 0 WHERE ID_FRIEND = @ID_FRIEND",
                    conn);

                    SqlParameter ID_FRIEND1 = new SqlParameter("@ID_FRIEND", System.Data.SqlDbType.Int);
                    ID_FRIEND1.Value = item.id;
                    cmdCoordnateOld.Parameters.Add(ID_FRIEND1);
                    cmdCoordnateOld.ExecuteNonQuery();

                    SqlCommand cmdCoordnateNew = new SqlCommand(@"
                        INSERT INTO TB_COORDINATE (ID_FRIEND, LATITUDE, LONGITUDE, [CURRENT]) VALUES (@ID_FRIEND, @LATITUDE, @LONGITUDE, 1)",
                    conn);
                    SqlParameter ID_FRIEND2 = new SqlParameter("@ID_FRIEND", System.Data.SqlDbType.Int);
                    ID_FRIEND2.Value = item.id;
                    cmdCoordnateNew.Parameters.Add(ID_FRIEND2);

                    SqlParameter LATITUDE = new SqlParameter("@LATITUDE", System.Data.SqlDbType.Float);
                    LATITUDE.Value = item.currentCoordinate.latitude;
                    cmdCoordnateNew.Parameters.Add(LATITUDE);

                    SqlParameter LONGITUDE = new SqlParameter("@LONGITUDE", System.Data.SqlDbType.Float);
                    LONGITUDE.Value = item.currentCoordinate.longitude;
                    cmdCoordnateNew.Parameters.Add(LONGITUDE);

                    cmdCoordnateNew.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
        }

        public void delete(FriendBo item)
        {
            SqlConnection conn = new SqlConnection(_strConn);
            conn.Open();
            try
            {
                using (conn)
                {
                    SqlCommand cmdCoordinate = new SqlCommand(@"
                        DELETE FROM TB_COORDINATE WHERE ID_FRIEND = @ID_FRIEND",
                    conn);
                    SqlParameter ID_FRIEND1 = new SqlParameter("@ID_FRIEND", System.Data.SqlDbType.Int);
                    ID_FRIEND1.Value = item.id;
                    cmdCoordinate.Parameters.Add(ID_FRIEND1);
                    cmdCoordinate.ExecuteNonQuery();

                    SqlCommand cmdFriend = new SqlCommand(@"
                        DELETE FROM TB_FRIEND WHERE ID = @ID_FRIEND",
                    conn);
                    SqlParameter ID_FRIEND2 = new SqlParameter("@ID_FRIEND", System.Data.SqlDbType.Int);
                    ID_FRIEND2.Value = item.id;
                    cmdFriend.Parameters.Add(ID_FRIEND2);
                    cmdFriend.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
        }

        private List<IFriend> mappingDaoToDomainList(List<FriendBo> listBo)
        {
            List<IFriend> list = new List<IFriend>();
            foreach (FriendBo item in listBo)
            {
                list.Add(new FriendDomain()
                {
                    id = item.id,
                    name = item.name,
                    currentCoordinate = (item.currentCoordinate != null) ? new CoordinateDomain()
                    {
                        latitude = item.currentCoordinate.latitude,
                        longitude = item.currentCoordinate.longitude,
                    } : null
                });
            }
            return list;
        }

        public static FriendBo MappingDomainToBo(FriendDomain domain)
        {
            FriendBo bo = new FriendBo()
            {
                id = domain.id,
                name = domain.name,
                currentCoordinate = (domain.currentCoordinate != null) ? new CoordinateBo()
                {
                    latitude = domain.currentCoordinate.latitude,
                    longitude = domain.currentCoordinate.longitude,
                } : null
            };

            return bo;
        }
    }
}
