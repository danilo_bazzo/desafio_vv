﻿IF EXISTS(SELECT 1 FROM SYS.DATABASES WHERE NAME = 'dbSQLFindFriends')
	DROP DATABASE dbSQLFindFriends
GO

IF EXISTS(select 1 from SYS.syslogins where name = 'Friends')
	DROP LOGIN Friends
GO

CREATE DATABASE dbSQLFindFriends
GO

CREATE LOGIN Friends 
    WITH PASSWORD    = N'Friends',
    CHECK_POLICY     = OFF,
    CHECK_EXPIRATION = OFF;
GO

--Obs.: Mapear o usuário Friends na base dbSQLFindFriends como db_owner