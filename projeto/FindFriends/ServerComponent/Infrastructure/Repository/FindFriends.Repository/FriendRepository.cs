﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;
using MongoDB.Driver;
using FindFriends.Repository.Model;
using FindFriends.Domain;
//using Microsoft.Extensions.Configuration;

namespace FindFriends.Repository
{
    public class FriendRepository
    {
        private IMongoDatabase mdDatabase;
        public FriendRepository(string connectionString)
        {
            try
            {
                //"mongodb://localhost:27017@dbFriends"
                string _strConn = connectionString.Split('@')[0]; 
                string _strDB = connectionString.Split('@')[1];

                var cliente = new MongoClient(_strConn);
                mdDatabase = cliente.GetDatabase(_strDB);
            }
            catch (MongoException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<IFriend> listCache()
        {
            try
            {
                var list = mdDatabase.GetCollection<FriendDocument>("Friend");
                var registrations = list.Find(i => i.name != string.Empty).ToListAsync();

                return mappingDocumentToDomainList(registrations.Result);
            }
            catch (MongoException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IFriend get(int id)
        {
            var list = mdDatabase.GetCollection<FriendDocument>("Friend");
            FriendDocument item = list.Find<FriendDocument>(x => x.id == id).Single();
            return mappingDocumentToDomain(item);
        }

        public void updateListCache(List<IFriend> listUpdate)
        {
            try
            {
                var list = mdDatabase.GetCollection<FriendDocument>("Friend");
                foreach(FriendDocument item in mappingDomainToDocumentList(listUpdate))
                {
                    list.FindOneAndDelete(x => x.id == item.id);
                    list.InsertOne(item);
                }
            }
            catch (MongoException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool updateCache(FriendDocument document)
        {
            try
            {
                var list = mdDatabase.GetCollection<FriendDocument>("Friend");
                list.FindOneAndDelete(x => x.id == document.id);
                list.InsertOne(document);

                return true;
            }
            catch (MongoException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool deleteCache(FriendDocument document)
        {
            try
            {
                var list = mdDatabase.GetCollection<FriendDocument>("Friend");
                list.FindOneAndDelete(x => x.id == document.id);

                return true;
            }
            catch (MongoException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private List<IFriend> mappingDocumentToDomainList(List<FriendDocument> listDoc)
        {
            List<IFriend> list = new List<IFriend>();
            foreach (FriendDocument item in listDoc)
            {
                list.Add(new FriendDomain()
                {
                    id = item.id,
                    name = item.name,
                    currentCoordinate = (item.currentCoordinate != null) ? new CoordinateDomain()
                    {
                        latitude = item.currentCoordinate.latitude,
                        longitude = item.currentCoordinate.longitude,
                    } : null
                });
            }
            return list;
        }

        private List<FriendDocument> mappingDomainToDocumentList(List<IFriend> listDomain)
        {
            List<FriendDocument> list = new List<FriendDocument>();

            foreach (FriendDomain item in listDomain)
                list.Add(MappingDomainToDocument(item));
            
            return list;
        }

        public static FriendDocument MappingDomainToDocument(FriendDomain domain)
        {
            FriendDocument doc = new FriendDocument()
                {
                    id = domain.id,
                    name = domain.name,
                    currentCoordinate = (domain.currentCoordinate != null) ? new CoordinateDocument()
                    {
                        latitude = domain.currentCoordinate.latitude,
                        longitude = domain.currentCoordinate.longitude,
                    } : null
                };

            return doc;
        }

        private IFriend mappingDocumentToDomain(FriendDocument doc)
        {
            IFriend domain = new FriendDomain()
            {
                id = doc.id,
                name = doc.name,
                currentCoordinate = (doc.currentCoordinate != null) ? new CoordinateDomain()
                {
                    latitude = doc.currentCoordinate.latitude,
                    longitude = doc.currentCoordinate.longitude,
                } : null
            };

            return domain;
        }
    }
}
