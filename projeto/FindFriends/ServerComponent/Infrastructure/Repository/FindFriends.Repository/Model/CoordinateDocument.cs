﻿using System;
using System.Collections.Generic;
using System.Text;
using FindFriends.Domain;

namespace FindFriends.Repository.Model
{
    public class CoordinateDocument : ICoordinate
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
