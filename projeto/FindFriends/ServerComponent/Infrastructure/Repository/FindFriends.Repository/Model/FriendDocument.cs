﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.ComponentModel.DataAnnotations;
using FindFriends.Domain;

namespace FindFriends.Repository.Model
{
    public class FriendDocument : IFriend
    {
        [BsonId]
        [Display(Name = "_id")]
        public MongoDB.Bson.ObjectId _id { get; set; }

        [BsonRequired]
        [Display(Name = "id")]
        public int id { get; set; }

        [BsonRequired]
        [Display(Name = "name")]
        public String name { get; set; }

        [BsonRequired]
        [Display(Name = "currentCoordinate")]
        public CoordinateDocument currentCoordinate { get; set; }
    }
}
