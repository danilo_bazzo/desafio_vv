﻿using System;
using System.Collections.Generic;
using FindFriends.Domain;
using FindFriends.Repository;
using FindFriends.Data;
using System.Threading.Tasks;
using FindFriends.Repository.Model;
using FindFriends.Data.Model;
using FindFriends.Domain.Functions;
using Microsoft.Extensions.Configuration;

namespace FindFriends.Application
{
    public class FriendsApp
    {
        private string _connectionStringSQL;
        private string _connectionStringMongo;

        private bool executeData;
        private bool executeDocument;

        public FriendsApp(string connectionStringSQL, string connectionStringMongo)
        {
            _connectionStringSQL = connectionStringSQL;
            _connectionStringMongo = connectionStringMongo;

            FriendsAppSetFlagExecute();
        }

        public FriendsApp(IConfiguration configuration)
        {
            _connectionStringSQL = configuration.GetConnectionString("dbSQLServer");
            _connectionStringMongo = configuration.GetConnectionString("dbMongo");

            FriendsAppSetFlagExecute();
        }

        private void FriendsAppSetFlagExecute()
        {
            if (!string.IsNullOrEmpty(_connectionStringSQL)) executeData = true;
            if (!string.IsNullOrEmpty(_connectionStringMongo)) executeDocument = true;
        }

        public List<IFriend> list()
        {
            try
            {
                List<IFriend> list = null;

                var mongoRep = (executeDocument) ? (new FriendRepository(_connectionStringMongo)) : null;
                var sqlDB = (executeData) ? (new FriendDao(_connectionStringSQL)) : null;

                if (executeDocument)
                    list = mongoRep.listCache();

                if (list == null || list.Count == 0)
                {
                    if (executeData)
                    {
                        list = sqlDB.list();

                        if (executeDocument)
                            mongoRep.updateListCache(list);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<IFriend> listFriends(int id, int kmDistance)
        {
            try
            {
                IFriend sel = get(id);

                List<IFriend> lsFriends = list();
                List<IFriend> listReturn = new List<IFriend>();

                foreach (IFriend friend in lsFriends)
                {
                    if (friend.id != id)
                    {
                        ((FriendDomain)friend).distanceFriend = CalculateCoordinates.CalculateNearby(((FriendDomain)sel).currentCoordinate, ((FriendDomain)friend).currentCoordinate);

                        if (((FriendDomain)friend).distanceFriend < kmDistance && ((FriendDomain)friend).distanceFriend > -kmDistance)
                        {
                            listReturn.Add(((FriendDomain)friend));
                        }
                    }
                }

                return listReturn;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IFriend get(int id)
        {
            try
            {
                var mongoRep = (executeDocument) ? (new FriendRepository(_connectionStringMongo)) : null;
                return (executeDocument) ? mongoRep.get(id) : null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool insert(IFriend item)
        {
            try
            {
                var mongoRep = (executeDocument) ? (new FriendRepository(_connectionStringMongo)) : null;
                var sqlDB = (executeData) ? (new FriendDao(_connectionStringSQL)) : null;

                if (executeData)
                    item.id = sqlDB.insert(FriendDao.MappingDomainToBo((FriendDomain)item));

                if (executeDocument) mongoRep.updateCache(FriendRepository.MappingDomainToDocument((FriendDomain)item));

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool update(IFriend item)
        {
            try
            {
                var mongoRep = (executeDocument) ? (new FriendRepository(_connectionStringMongo)) : null;
                var sqlDB = (executeData) ? (new FriendDao(_connectionStringSQL)) : null;

                if ((executeDocument) ? mongoRep.updateCache(FriendRepository.MappingDomainToDocument((FriendDomain)item)) : true)
                {
                    if (executeData)
                    {
                        Task t = Task.Run(() => { sqlDB.update(FriendDao.MappingDomainToBo((FriendDomain)item)); });
                        t.Wait();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool delete(IFriend item)
        {
            try
            {
                var mongoRep = (executeDocument) ? (new FriendRepository(_connectionStringMongo)) : null;
                var sqlDB = (executeData) ? (new FriendDao(_connectionStringSQL)) : null;

                if ((executeDocument) ? mongoRep.deleteCache(FriendRepository.MappingDomainToDocument((FriendDomain)item)) : true)
                {
                    if (executeData)
                    {
                        Task t = Task.Run(() => { sqlDB.delete(FriendDao.MappingDomainToBo((FriendDomain)item)); });
                        t.Wait();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
