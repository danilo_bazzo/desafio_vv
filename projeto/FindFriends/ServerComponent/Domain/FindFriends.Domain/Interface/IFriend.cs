﻿using System;
using System.Collections.Generic;

namespace FindFriends.Domain
{
    public interface IFriend
    {
        int id { get; set; }
        string name { get; set; }
    }
}
