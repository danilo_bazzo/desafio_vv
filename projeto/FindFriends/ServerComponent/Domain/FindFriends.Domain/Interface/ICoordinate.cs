﻿using System;

namespace FindFriends.Domain
{
    public interface ICoordinate
    {
        double latitude { get; set; }
        double longitude { get; set; }
    }
}
