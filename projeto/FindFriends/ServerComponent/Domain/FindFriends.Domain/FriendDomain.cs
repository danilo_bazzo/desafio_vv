﻿using System;
using System.Collections.Generic;
using FindFriends.Domain;

namespace FindFriends.Domain
{
    public class FriendDomain : IFriend
    {
        public int id { get; set; }
        public string name { get; set; }

        public ICoordinate currentCoordinate { get; set; }

        public double distanceFriend { get; set; }
    }
}
