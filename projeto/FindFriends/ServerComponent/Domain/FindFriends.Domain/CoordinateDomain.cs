﻿using System;
using FindFriends.Domain;

namespace FindFriends.Domain
{
    public class CoordinateDomain : ICoordinate
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
