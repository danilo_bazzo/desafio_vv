﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FindFriends.Domain.Functions
{
    public class TokenValidation
    {
        public static string GetToken()
        {
            return DateTime.Now.ToString("yyyyMMddHHmm");
        }

        public static bool ValidToken(string token)
        {
            DateTime dt = DateTime.Now;
            DateTime dtMin = DateTime.Now.AddMinutes(-1);

            return token == dt.ToString("yyyyMMddHHmm") || 
                   token == dtMin.ToString("yyyyMMddHHmm");
        }
    }
}
