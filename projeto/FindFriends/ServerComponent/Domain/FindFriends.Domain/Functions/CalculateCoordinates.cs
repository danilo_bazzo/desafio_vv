﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FindFriends.Domain.Functions
{
    public class CalculateCoordinates
    {
        public static double CalculateNearby(ICoordinate cOrig, ICoordinate cFriend)
        {
            double cF = 90 - (cFriend.longitude);
            double cO = 90 - (cOrig.longitude);

            double dLat = cFriend.latitude - cOrig.latitude;

            double cosA =  Math.Cos(cO) * Math.Cos(cF) +
                            Math.Sin(cF) * Math.Sin(cO) * 
                            Math.Cos(dLat);

            double arcCos = Math.Acos(cosA);

            // 2 * pi * Raio da Terra = 6,28 * 6.371 = 40.030 Km 
            // 360 graus = 40.030 Km 
            // 3,2169287 = x 
            // x = (40.030 * 3,2169287)/360 = 357,68 Km
            double tt = (40030 * arcCos) / 360;

            if (tt < 0) tt = tt * -1;

            return tt;
        }
    }
}
