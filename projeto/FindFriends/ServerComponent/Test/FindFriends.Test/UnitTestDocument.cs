using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FindFriends.Test
{
    [TestClass]
    public class UnitTestDocument
    {
        private string mongo = "mongodb://localhost:27017@dbFriends";

        [TestMethod]
        public void TestMethod_insert()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(null, mongo);

            Domain.IFriend friend = new Domain.FriendDomain()
            {
                name = "Antonio",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -20.4560684,
                    longitude = -40.0866788
                }
            };

            bool testReturn = app.insert(friend) && 
                              app.delete(friend);//Remover item inserido no teste

            Assert.IsTrue(testReturn);

            
        }

        [TestMethod]
        public void TestMethod_update()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(null, mongo);

            Domain.IFriend friend = new Domain.FriendDomain()
            {
                id = 20,
                name = "Ricardo",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -20.4560684,
                    longitude = -40.0866788
                }
            };

            bool testReturn = app.update(friend)
                           && app.delete(friend);

            Assert.IsTrue(testReturn);
        }

        [TestMethod]
        public void TestMethod_delete()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(null, mongo);

            Domain.IFriend friend = new Domain.FriendDomain()
            {
                id = 21,
                name = "Mois�s",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -20.4560684,
                    longitude = -40.0866788
                }
            };

            bool testReturn = app.insert(friend)
                           && app.delete(friend);

            Assert.IsTrue(testReturn);
        }

        [TestMethod]
        public void TestMethod_list()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(null, mongo);

            Domain.IFriend friend = new Domain.FriendDomain()
            {
                name = "Andr�",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -23.4560684,
                    longitude = -48.0866788
                }
            };

            bool testReturn = app.update(friend);

            testReturn = testReturn && (app.list().Count > 0);

            testReturn = testReturn && app.delete(friend);//Remover item inserido no teste

            Assert.IsTrue(testReturn);
        }
    }
}
