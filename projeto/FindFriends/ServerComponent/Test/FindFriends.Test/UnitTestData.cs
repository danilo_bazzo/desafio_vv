using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FindFriends.Test
{
    [TestClass]
    public class UnitTestData
    {
        private string sql = "Persist Security Info=False;User ID=Friends;Password=Friends;Initial Catalog=dbSQLFindFriends;Data Source=.;";

        [TestMethod]
        public void TestMethod_insert()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(sql, null);

            Domain.IFriend friend = new Domain.FriendDomain()
            {
                name = "Antonio",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -20.4560684,
                    longitude = -40.0866788
                }
            };

            bool testReturn = app.insert(friend) &&
                              app.delete(friend); //Remover item inserido no teste

            Assert.IsTrue(testReturn);
        }

        [TestMethod]
        public void TestMethod_update()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(sql, null);

            Domain.IFriend friend = new Domain.FriendDomain()
            {
                name = "Ricardo",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -20.4560684,
                    longitude = -40.0866788
                }
            };

            bool testReturn = app.insert(friend)
                           && app.update(friend)
                           && app.delete(friend);

            Assert.IsTrue(testReturn);
        }

        [TestMethod]
        public void TestMethod_delete()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(sql, null);

            Domain.IFriend friend = new Domain.FriendDomain()
            {
                name = "Mois�s",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -20.4560684,
                    longitude = -40.0866788
                }
            };

            bool testReturn = app.insert(friend)
                           && app.delete(friend);

            Assert.IsTrue(testReturn);
        }

        [TestMethod]
        public void TestMethod_list()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(sql, null);

            Assert.IsTrue(app.list().Count > 0);
        }
    }
}
