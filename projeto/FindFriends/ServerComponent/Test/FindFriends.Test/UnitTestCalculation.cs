using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace FindFriends.Test
{
    [TestClass]
    public class UnitTestCalculation
    {
        private string sql = "Persist Security Info=False;User ID=Friends;Password=Friends;Initial Catalog=dbSQLFindFriends;Data Source=.;";
        private string mongo = "mongodb://localhost:27017@dbFriends";

        [TestMethod]
        public void TestMethod_Calculation()
        {
            FindFriends.Application.FriendsApp app = new Application.FriendsApp(sql, mongo);

            Domain.IFriend friendA = new Domain.FriendDomain()
            {
                name = "Cibele",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -93.530732,
                    longitude = -67.2697
                }
            };
            app.insert(friendA);

            Domain.IFriend friendB = new Domain.FriendDomain()
            {
                name = "Suzana",
                currentCoordinate = new Domain.CoordinateDomain()
                {
                    latitude = -93.503568,
                    longitude = -67.448241
                }
            };
            app.insert(friendB);

            List<Domain.IFriend> lsA = app.listFriends(friendA.id, 20);
            List<Domain.IFriend> lsB = app.listFriends(friendA.id, 1);

            app.delete(friendA);
            app.delete(friendB);

            Assert.IsTrue(lsA.Count == 1 && lsB.Count == 0);

        }
    }
}
