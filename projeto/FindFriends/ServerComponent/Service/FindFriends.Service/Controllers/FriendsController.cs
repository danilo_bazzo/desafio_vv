﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using FindFriends.Domain;
using FindFriends.Application;
using Microsoft.Extensions.Configuration;
using FindFriends.Service.Security;

namespace FindFriends.Service.Controllers
{
    [Route("api/friends")]
    [ApiController]
    [ValidToken()]
    public class FriendsController : ControllerBase
    {
        private IConfiguration _configuration;

        public FriendsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET api/friend
        [HttpGet(Name = "list")]
        public ActionResult<IEnumerable<IFriend>> list()
        {
            return new FriendsApp(_configuration).list();
        }

        // GET api/friend/5/10
        [HttpGet("{id}/{kmDistance}", Name = "listFriends")]
        public ActionResult<IEnumerable<IFriend>> listFriends(int id, int kmDistance)
        {
            return new FriendsApp(_configuration).listFriends(id, kmDistance);
        }

        // POST api/values
        [HttpPost(Name = "insert")]
        public void insert([FromBody] IFriend value)
        {
            new FriendsApp(_configuration).insert(value);
        }

        // PUT api/values/5
        [HttpPut("{id}", Name = "update")]
        public void update(int id, [FromBody] IFriend value)
        {
            value.id = id;
            new FriendsApp(_configuration).update(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}", Name = "delete")]
        public void delete(int id)
        {
            new FriendsApp(_configuration).delete(new FriendDomain() { id = id });
        }
    }
}
